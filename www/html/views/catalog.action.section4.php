<?php

	$alias = 'nam-doveryayut';

	$table = new Table( 'catalog_section' );
	
	$section = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $alias ) );
	if ( !count( $section ) ) return false;
	$section = end( $section );

	$section_parents = $table -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:pid ORDER BY `position`', array( 'pid' => $section[ 'id' ] ) );
	if ( !count( $section_parents ) ) return false;
	
	
	foreach ( $section_parents as $key => $section_parent ) {

		$section_page = $table -> select( 'SELECT * FROM `' . $section_parent[ 'section_table' ] . '` WHERE `id`=:id LIMIT 1', array( 'id' => $section_parent[ 'id' ] ) );
		if ( !count( $section_page ) ) return false;
		$section_page = end( $section_page );

		
		echo '
		<div class="col-xs-12 col-sm-6 col-md-4">
			<a href="#"><img src="/' . $section_page[ 'img' ] . '"></a>
		</div>';


	}
	