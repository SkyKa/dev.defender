<?php

	$alias = 'osporit-kadastrovuyu-stoimost-nedvizhimosti';

	$table = new Table( 'catalog_section' );
	
	$section = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $alias ) );
	if ( !count( $section ) ) return false;
	$section = end( $section );

	$section_page = $table -> select( 'SELECT * FROM `' . $section[ 'section_table' ] . '` WHERE `id`=:id LIMIT 1', array( 'id' => $section[ 'id' ] ) );
	if ( !count( $section_page ) ) return false;
	$section_page = end( $section_page );

?>


		<img src="<?php echo '/'. get_cache_pic( $section_page[ 'img' ], 1903, 520, true )?>" class="wraper-background">
		<div class="container">
			<div class="col-xs-12 wrap-title">
				<?php mod( 'pages.show.content' )?>
			</div>
			<div class="col-xs-12 col-sm-5 wrap-block1">
				<?php echo $section_page[ 'small_text' ]?>
			</div>
			<div class="col-xs-0 col-sm-1"></div>
			<div class="col-xs-12 col-sm-6 wrap-block2">
				<div class="col-xs-12 col-md-7 zvon">
					<a href="tel:<?php echo Utils::phone_number( val( 'banner.show.phone' ) )?>"><?php mod( 'banner.show.phone' )?></a>
				</div>
				<div class="col-xs-12 col-md-5 knop">
					<a href="tel:<?php echo Utils::phone_number( val( 'banner.show.phone' ) )?>">ПОЗВОНИТЬ</a>
				</div>
				<?php echo $section_page[ 'content' ]?>
				</span>
			</div>
		</div>