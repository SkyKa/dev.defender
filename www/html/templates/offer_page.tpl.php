<?php include "html/templates/header.tpl.php" ?>


	<div class="container-fluid">
	<div class="row wraper_back_hidden">
		<?php mod( 'catalog.action.section1' );?>
	</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="container wrap2" id="wrap2">
				<?php mod( 'catalog.action.section2' )?>
			</div>
		</div>
	</div>
	<div class="container-fluid">
	<div class="row wrap3">
	<div class="container">
		<h2>Часто задаваемые вопросы</h2>
		<div class="col-xs-12">
			<?php mod( 'catalog.action.section3' )?>
		</div>
		<div class="col-xs-12">
			<div class="wrap3-button"><button data-toggle="modal" data-target="#myModal">ЗАДАТЬ ВОПРОС СПЕЦИАЛИСТУ</button></div>
		</div>
	</div>
	</div>
	</div>
	<div class="container-fluid">
	<div class="row wrap4">
	<div class="container">
		<div class="col-xs-12 telephone">
			<h2>Задать вопрос по телефону</h2>
			<a href="tel:<?php echo Utils::phone_number( val( 'banner.show.phone' ) )?>"><?php mod( 'banner.show.phone' )?></a>
		</div>
	</div>
	</div>
	</div>
	<div class="container-fluid">
	<div class="row">
	<div class="container jdun" id="jdun">
	<h3>Нам доверяют</h3>
	<?php mod( 'catalog.action.section4' )?>
	
	</div>
	</div>
	</div>
	<div class="container-fluid">
	<div class="row coop">
	<div class="container" id="coop">
		<h2>Как происходит сотрудничество</h2>
		<ul class="coop-ul">
			<div class="line-god">
			<div class="line"></div>
			<?php mod( 'catalog.action.section5' )?>
		</ul>
	</div>
	</div>
	</div>
	<div class="container-fluid">
	<div class="row anketa_back_hidden">
	<img src="/static/img/anketa_back.png" class="anketa_back">
	<div class="container" id="anketa">
		<div class="col-xs-12 anketa">
			<h2>Заполните заявку</h2>
			<p>Мы подготовим предложение по оспариванию кадастровой стоимости<br>
			в течение 24 часов</p>
			<?php mod( 'catalog.action.forms', array( 'alias' => 'zayavka' ) )?>
		</div>		
	</div>
	</div>
	</div>


<?php include "html/templates/offer_footer.tpl.php" ?>
