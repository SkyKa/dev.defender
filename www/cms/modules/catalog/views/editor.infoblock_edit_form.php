{
    layout:'fit',
    width:600,
    height:500,
    plain: true,
    border: false,
    title: <?php echo escapeJSON('Редактирование')?>,
    items:
    [
        {
            xtype: 'form',
            itemId: 'infoblock-form-<?php echo $infoblock->id?>',
            frame: true,
            autoScroll: true,
            labelAlign: 'top',
            defaults:
            {
                width: 350,
                xtype: 'textfield'
            },
            items:
            [
                {
                    xtype: 'hidden',
                    name: 'id',
                    value: '<?php echo $infoblock->id?>'
                },
                {

                    xtype: 'panel',
                    itemId: 'panel-content',
                    fieldLabel: 'Содержание',
                    height: 340,
                    width: '99%',
                    autoScroll: true,
                    bodyStyle: 'background-color: #fff; padding: 8px',
                    html: <?php echo escapeJSON($infoblock->html) ?>,
                    tbar:
                    {
                        xtype: 'toolbar',
                        items:
                        [
                            {
                                text:'Правка',
                                iconCls: 'edit-menu',
                                handler: function(btn){
                                        var textarea =
                                        {
                                            xtype: 'textarea',
                                            fieldLabel: 'Текст',
											id: 'infoblock-form-textarea-<?php echo $infoblock->id?>',
                                            name: 'html',
                                            height : 350,
                                            width : 600,
                                            value : <?php echo escapeJSON($infoblock->html) ?>,
                                            listeners:
                                            {
                                                render : function(element)
                                                    {
                                                        oFCKeditor = CKEDITOR.replace( element.id );
                                                    }
                                             }
                                        };
										var panel = this.ownerCt.ownerCt;
                                        panel.setHeight('auto');
                                        panel.add(textarea);
                                        panel.body.update('');
                                        panel.body.setStyle('background-color','');
                                        panel.getTopToolbar().hide();
                                        panel.doLayout();
                                    }
                            }
                        ]
                    }
                }
            ]
        }
    ],
    buttonAlign: 'center',
    buttons:
    [
        {
            text:'Сохранить',
            handler: function(btn)
            {

                var win = this.ownerCt.ownerCt;
                win.hide();
                var form = win.getComponent('infoblock-form-<?php echo $infoblock->id?>');
                var ta = form.getForm().findField('html');
                if(ta)
                {
                    var val = CKEDITOR.instances[ta.getId()].getData();
                    ta.setValue(val);
                }

                form.getForm().submit({
                    url: '/ajax/mysite/catalog.editor.save_infoblock',
                    method: 'POST',
                    waitTitle: 'Подождите',
                    waitMsg: 'Сохранение...',
                    success: function(form, action){
                        window.location = window.location;
                        win.close();
                    },
                    failure: function(form, action){
                        Ext.MessageBox.alert('Ошибка', action.result.msg);
                        win.close();
                    }
                });
            }
        },
        {
            text: 'Отмена',
            handler: function()
            {
                this.ownerCt.ownerCt.close();
            }
        }
    ]
}
