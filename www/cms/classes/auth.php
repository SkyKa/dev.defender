<?php

class Auth {

	public $errorInfo = array( ); // ошибки внутри класса
	public $out = array( ); // ошибки выводятся в вывод
	
	protected $table = 'position_learner';

	protected static $_instance;
	protected static $_sole = 'JDH878dDDTygdyyft&Ff4tfyLKfjH760875F6s5dfGFfFl393034';



	public static function getInstance( ) {
		if ( null === self :: $_instance ) {
			self :: $_instance = new self( );
		}
		return self :: $_instance;
	}



	public function __destruct( ) {
		//Registry :: __instance( ) -> errorOutput = $out;
	}


	// авторизация
	// email, pwd
	public function login( $args=array( ) ) {

		if ( !count( $args ) ) {
			$this -> errorInfo[ ] = __FUNCTION__ . " : " . __LINE__ . " > Не переданы аргументы";
			return false;
		}

		foreach ( $args as $key => $arg ) {
			$$key = $arg;
		}

		$table = new Table( $this -> table );
		$rows = $table -> select( "SELECT * FROM  `" . $this -> table . "` WHERE `email`=:email LIMIT 1",
				array( 'email' => $email ) );
		if ( count( $rows ) ) {
			$row = $rows[ 0 ];
			if ( $row[ 'pass' ] == md5( $pass ) ) {
				$_SESSION[ 'sess' ] = $row;
				$_SESSION[ 'sess' ][ 'owner' ] = 'self';
				unset( $_SESSION[ 'sess' ][ 'pass' ] );
			}
			// неверный пароль
			else {
				$this -> _out( 'system', 'Неверный Логин/Пароль' );
			}
			if ( !$row[ 'visible' ] ) {
				$this -> _out( 'system', 'Учетная запись не активирована, проверьте почту.' );
			}
		}
		// неверный логин
		else {
			$this -> _out( 'system', 'Неверный Логин/Пароль' );
		}

		if ( !$this -> _out( ) ) {
			$this -> _out( 'ok',
			"Регистрация прошла успешно! теперь необходимо активировать учетную запись.<br />
			Инструкции по активации была выслана Вам на электронную почту." );
			return true;
		}
		return false;

	}


	// авторизация админа
	public function loginAdm( $id ) {

		$login = val( 'security.login' );
	
		if ( !$login ) {
			$this -> errorInfo[ ] = __FUNCTION__ . " : " . __LINE__ . " > Пользователь не авторизован в cms";
			return false;
		}

		if ( $this -> isAuth( ) ) {
			$this -> _out( 'system', 'На данный момент вы уже авторизованы' );
			return false;
		}

		$row = $this -> getUserById( $id );

		if ( $row ) {
			$_SESSION[ 'sess' ] = $row;
			$_SESSION[ 'sess' ][ 'owner' ] = $login;
			unset( $_SESSION[ 'sess' ][ 'pass' ] );
			$this -> _out( 'ok', "Вы авторизованы!" );
			return true;
		}

		$this -> _out( 'system', 'Пользователь не найден' );
		return false;
	}


	// выход
	public function logout( ) {
		unset( $_SESSION[ 'sess' ] );
	}


	// проверка на авторизацию
	public function isAuth( ) {
		if ( isset( $_SESSION[ 'sess' ] ) ) {
			return $_SESSION[ 'sess' ];
		}
		else return false;
	}


	// получаем section_id пользователей
	public function usersSid( ) {
		$table = new Table( $this -> table );
		$rows = $table -> select( 'SELECT `id` as `s_id`, `position_table`
		FROM `catalog_section` WHERE `alias`=:alias &&
		`position_table`="' . $this -> table . '" LIMIT 1', array( 'alias' => 'users' ) );
		if ( !count( $rows ) ) {
			$this -> errorInfo[ ] = __FUNCTION__ . " : " . __LINE__ . " > not alias users from catalog_section table";
			return false;
		}
		return $rows[ 0 ][ 's_id' ];
	}

	// ok
	public function outOk( ) {
		$arr = $this -> out;
		if ( !count( $arr ) ) {
			if ( isset( $_SESSION[ 'out' ] ) ) $arr = $_SESSION[ 'out' ];
		}
		foreach ( $arr as $key => $a ) {
			if ( $key != 'ok' ) {
				unset( $arr[ $key ] );
			}
		}
		return $arr;
	}

	// system
	public function outSystem( ) {
		$arr = $this -> out;
		if ( !count( $arr ) ) {
			if ( isset( $_SESSION[ 'out' ] ) ) $arr = $_SESSION[ 'out' ];
		}
		foreach ( $arr as $key => $a ) {
			if ( $key != 'system' ) {
				unset( $arr[ $key ] );
			}
		}
		return $arr;
	}

	// other rows
	public function arrayOutRows( ) {
		$arr = $this -> out;
		if ( !count( $arr ) ) {
			if ( isset( $_SESSION[ 'out' ] ) ) $arr = $_SESSION[ 'out' ];
		}
		unset( $arr[ 'ok' ] );
		unset( $arr[ 'system' ] );
		return $arr;
	}
	
	
	// получить пользователя по идентификатору
	public function getUserById( $id ) {
		$table = new Table( $this -> table );
		$rows = $table -> select( 'SELECT * FROM `' . $this -> table . '` WHERE `id`=:id LIMIT 1',
			array( 'id' => $id ) );
		if ( count( $rows ) ) return end( $rows );
		else return false;
	}
	

	// вычисляем HASH сумму событий
	// email	-	электронная почта
	// action	-	действие пользователя
	protected function _hash( $email, $action ) {
		return md5( $email . $action . self :: $_sole );
	}

	protected function _out( $key=null, $val=null ) {
		if ( !isset( $_SESSION[ 'out' ] ) ) $_SESSION[ 'out' ] = array( );
		if ( $key && $val ) {
			$this -> out[ $key ][ ] = $val;
			$_SESSION[ 'out' ][ $key ][ ] = $val;
		}
		else if ( $key ) return count( $this -> out[ $key ] );
		else return count( $this -> out );
	}
	


	
	
	
	
	
	// Ф О Р М Ы
	
	// -- Д О   А В Т О Р И З А Ц И И


	// регистрация
	
	// name			- имя
	// sname		- фамилия
	// phone		- телефон
	// email		- e-mail
	// pwd			- пароль
	// pwd2			- подтверждение пароля
	// date			- дата рождения
	// photo		- фотография
	// promo		- промокод
	// send_news	- подписка на новости
	// consent		- согласие с условиями пользовательского соглашения
	public function register( $args=array( ) ) {

		global $site_name, $email_admin;

		if ( !count( $args ) ) {
			$this -> errorInfo[ ] = __FUNCTION__ . " : " . __LINE__ . " > not alias users from catalog_section table";
			return false;
		}

		$action = 'register';

		foreach ( $args as $key => $arg ) {
			$$key = $arg;
		}

		if ( !$name ) $this -> _out( 'name', "Укажите имя" );
		if ( !$sname ) $this -> _out( 'sname', "Укажите фамилию" );
		if ( !$phone ) $this -> _out( 'phone', "Укажите телефон" );
		if ( !$email ) $this -> _out( 'email', "Укажите адрес электронной почты" );
		else if ( !preg_match( "/[0-9a-zA-Z_\-]+@[0-9a-zA-Z_\.\-]+\.[a-zA-Z_]{2,3}/i", $email ) )
			$this -> _out( 'email', "Адрес электронной почты введен неверно" );

		// if ( !preg_match( "/^[0-9]$/i", $date ) ) $err .= "Укажите дату рождения";
		// if ( !$photo ) $err .= "Загрузите свою фотографию";

		if ( !$pwd ) $this -> _out( 'pwd', "Установите пароль" );
		if ( mb_strlen( $pwd ) < 5 ) $this -> _out( 'pwd', "Пароль не должен быть менее 5ти символов" );
		if ( $pwd != $pwd2 ) $this -> _out( 'pwd2', "Пароль и подтверждение пароля не совпадают" );
		if ( !$consent ) $this -> _out( 'system', "Вам необходимо согласиться с условиями пользовательского соглашени" );
		$send_news = ( !$send_news ) ? 0 : 1;

		// -- АВТОМАТИЧЕСКАЯ ВАЛИДАЦИЯ
		//$form = new Form( $action );
		//$this -> errorOutput = $form -> valide( );

		$table = new Table( $this -> table );
		$intent = $table -> select( "SELECT * FROM  `" . $this -> table . "` WHERE `email`=:email LIMIT 1",
							array( 'email' => $email ) );

		if ( count( $intent ) ) {
			$intent = $intent[ 0 ];
			$this -> _out( 'system', 'На этот адрес электронной почты уже есть зарегистрированный аккаунт' );
			return false;
		}

		if ( !$this -> _out( ) ) {

			$insert							= $table -> getEntity( );
			$insert -> section_id 			= $this -> usersSid( );
			$insert -> title				= $name;
			$insert -> stitle				= $sname;
			$insert -> phone				= $phone;
			$insert -> email				= $email;
			$insert -> pass					= md5( $pwd );
			$insert -> hb					= $date;
			$insert -> photo				= $photo;
			$insert -> promo				= $promo;
			$insert -> send_news			= $send_news;
			$insert -> visible				= 0;

			$table -> save( $insert );
			if ( !$table -> errorInfo ) {

				$hash = $this -> _hash( $email, $action );

				$body = "
" . $name . " " . $sname . ", добрый день!<br />
Вы получили это письмо, потому что ваш электронный ящик был указан при регистрации на сайте \"" . $site_name . "\",<br />
если вы не регистрировались, просто проигнорируйте данное письмо.<br /><br />
Для активации зарегистрированной учетной записи пройдите по ссылке:<br />
<a href='http://" . Utils :: getHost( ) . "/auth.html?form_action=activation&email=" . $email . "&hash=" . $hash . "' target='_blank'>http://" . Utils :: getHost( ) . "/login.html?hash=" . $hash . "</a>
<br />
<br />
Благодарим Вас за регистрацию.<br />
С уважением, администрация сервиса " . $site_name . ".";

				$body = self :: mailTpl( $body );
				
				
				$mailer = new Mailer( );
				$mailer	-> IsHTML( true );
				$mailer -> From			= $email_admin;
				$mailer -> FromName		= $site_name;
				$mailer -> Subject		= $site_name . " - регистрация учетной записи.";
				$mailer -> Body			= $body;

				$mailer -> AddAddress( trim( $email ) );

				if ( !$mailer -> Send( ) ) {
					$this -> _out( 'system', 'На этот адрес электронной почты уже есть зарегистрированный аккаунт' );
					return false;
				}

			}

		}
		if ( !$this -> _out( ) ) {
			$this -> _out( 'ok',
			"Регистрация прошла успешно! теперь необходимо активировать учетную запись.<br />
			Инструкции по активации была выслана Вам на электронную почту." );
			return true;
		}
		return false;
	}

	

	// активация учетной записи
	public function activation( $args ) {

		global $site_name, $email_admin;

		if ( !count( $args ) ) {
			$this -> errorInfo[ ] = __FUNCTION__ . " : " . __LINE__ . " > Не переданы аргументы";
			return false;
		}

		$action = 'activation';

		foreach ( $args as $key => $arg ) {
			$$key = $arg;
		}

		if ( !$email ) $this -> _out( 'email', "Укажите адрес электронной почты" );
		else if ( !preg_match( "/[0-9a-zA-Z_\-]+@[0-9a-zA-Z_\.\-]+\.[a-zA-Z_]{2,3}/i", $email ) )
			$this -> _out( 'email', "Адрес электронной почты введен неверно" );

		// $form = new Form( $action );
		// $this -> errorOutput = $form -> valide( );

		if ( !$this -> _out( ) ) {

			$hash = $this -> _hash( $email, $action );

			$table = new Table( $this -> table );

			$rows = $table -> select( "SELECT * FROM  `" . $this -> table . "`
					WHERE MD5( CONCAT(`email`,'" . $action . self :: $_sole . "' ) )=:hash LIMIT 1",

			array( 'hash' => $hash ) );
			if ( count( $rows ) ) {
				$row = $rows[ 0 ];

				$table							= new Table( $this -> table );
				$insert							= $table -> getEntity( $row[ 'id' ] );
				$insert -> section_id 			= $this -> usersSid( );
				$insert -> visible				= 1;

				$table -> save( $insert );
				if ( !$table -> errorInfo ) {

					$body = "
Ваша учетная запись, успешно активирована!
<br />
Чтобы авторизоваться пройдите по ссылке:<br />
<a href='http://" . Utils :: getHost( ) . "/auth.html'>http://" . Utils :: getHost( ) . "/auth.html</a>
<br />
<br />
Благодарим Вас за регистрацию.<br />
С уважением, администрация сервиса " . $site_name . ".";
				
					$body = self :: mailTpl( $body );
				
					$mailer = new Mailer( );
					$mailer	-> IsHTML( true );
					$mailer -> From			= $email_admin;
					$mailer -> FromName		= $site_name;
					$mailer -> Subject		= $site_name . " - активация учетной записи.";
					$mailer -> Body			= $body;

					$mailer -> AddAddress( trim( $email ) );

					if ( !$mailer -> Send( ) ) {
						$this -> _out( 'system', "Ошибка: попробуйте повторить это действие через некоторое время" );
						return false;
					}
				}
			}
		}
		if ( !$this -> _out( ) ) {
			$this -> _out( 'ok',
			"Активация учетной записи прошла успешно! Теперь вы можете авторизоваться в системе." );
			return true;
		}
	}

	
	// повторная отправка письма с активацией учетной записи
	public function noRegisterEmail( ) {

		global $site_name;

		if ( !count( $args ) ) {
			$this -> errorInfo[ ] = __FUNCTION__ . " : " . __LINE__ . " > Не переданы аргументы";
			return false;
		}

		foreach ( $args as $key => $arg ) {
			$$key = $arg;
		}

		if ( !$email ) $this -> _out( 'email', "Укажите адрес электронной почты" );
		else if ( !preg_match( "/[0-9a-zA-Z_\-]+@[0-9a-zA-Z_\.\-]+\.[a-zA-Z_]{2,3}/i", $email ) )
			$this -> _out( 'email', "Адрес электронной почты введен неверно" );

		if ( !$this -> _out( ) ) {
		
			$table = new Table( $this -> table );
			$rows = $table -> select( "SELECT * FROM  `" . $this -> table . "` WHERE `email`=:email LIMIT 1",
								array( 'email' => $email ) );

			if ( count( $rows ) ) {
				$row = $rows[ 0 ];
				$visible = ( isset( $row[ 'visible' ] ) && $row[ 'visible' ] ) ? 1 : 0;
				if ( $visible ) {
					$this -> _out( 'system', "Учетная запись уже активирована" );
					return false;
				}

				$hash = $this -> _hash( $email, 'no-email' );

					$body = "
Добрый день " . $row['title'] . " " . $row['stitle'] . "!<br />
Вы получили это письмо, потому что ваш электронный ящик был указан при регистрации на сайте \"" . $site_name . "\",<br />
если вы не регистрировались, просто проинорируйте данное письмо.<br /><br />
Для активации зарегистрированной учетной записи пройдите по ссылке:<br />
<a href='http://" . Utils :: getHost( ) . "/?link_action=no-email&hash=" . $hash . "' target='_blank'>http://" . Utils :: getHost( ) . "/?link_action=no-email&hash=" . $hash . "</a>
<br />
<br />
Благодарим Вас за регистрацию.<br />
С уважением, администрация сервиса " . $site_name . ".";
				
					$body = self :: mailTpl( $body );
				
				
				$mailer 						= new Mailer( );
				$mailer	-> IsHTML( true );
				$mailer -> From			= $email_admin;
				$mailer -> FromName		= $site_name;
				$mailer -> Subject		= "Регистрация на сайте " . $site_name;
				$mailer -> Body			= $body;

				$mailer -> AddAddress( trim( $email ) );

				if ( !$mailer -> Send( ) ) {
					$this -> _out( 'system', "Ошибка: попробуйте повторить это действие через некоторое время" );
				}
			}
			else {
				$this -> _out( 'system', "Пользователь с таким адресом электронной почты не зарегистрирован" );
			}
		}

		if ( !$this -> _out( ) ) {
			$this -> _out( 'ok',
			"Регистрация прошла успешно! теперь необходимо активировать учетную запись.<br />
			Инструкции по активации была выслана Вам на электронную почту." );
			return true;
		}
		return false;
	}


	// забыли пароль?
	public function forgotPass( $args ) {

		global $site_name, $email_admin;

		if ( !count( $args ) ) {
			$this -> errorInfo[ ] = __FUNCTION__ . " : " . __LINE__ . " > Не переданы аргументы";
			return false;
		}

		foreach ( $args as $key => $arg ) {
			$$key = $arg;
		}

		if ( !$email ) $this -> _out( 'email', "Укажите адрес электронной почты" );
		else if ( !preg_match( "/[0-9a-zA-Z_\-]+@[0-9a-zA-Z_\.\-]+\.[a-zA-Z_]{2,3}/i", $email ) )
			$this -> _out( 'email', "Адрес электронной почты введен неверно" );

		if ( !$this -> _out( ) ) {
		
			$table = new Table( $this -> table );
			$rows = $table -> select( "SELECT * FROM  `" . $this -> table . "` WHERE `email`=:email LIMIT 1",
								array( 'email' => $email ) );
			if ( count( $rows ) ) {

				$row = $rows[ 0 ];

				$hash = $this -> _hash( $email, date( 'U' ) );

				$body = "
Для восстановления доступа к своей учетной записи пройдите по ссылке:<br />
<a href='http://" . Utils :: getHost( ) . "/change-pass.html?form_action=change-pass&hash=" . $hash . "' target='_blank'>http://" . Utils :: getHost( ) . "/login.html?hash=" . $hash . "</a>
<br />
<br />
С уважением, администрация сервиса " . $site_name . ".";
			
				$body = self :: mailTpl( $body );
				
				
				$mailer 				= new Mailer( );
				$mailer	-> IsHTML( true );
				$mailer -> From			= $email_admin;
				$mailer -> FromName		= $site_name;
				$mailer -> Subject		= $site_name . " - восстановление доступа.";
				$mailer -> Body			= $body;

				$mailer -> AddAddress( trim( $email ) );

				if( !$mailer -> Send( ) ) {
					$this -> _out( 'system', 'Ошибка: попробуйте повторить это действие через некоторое время' );
				}
				else {
					$table							= new Table( 'change_pwd' );
					$insert							= $table -> getEntity( );
					$insert -> email				= $email;
					$insert -> hash					= $hash;
					$insert -> datestamp			= time( ) + 3600; // смена пароля действует 1 час
					$insert -> ip					= $_SERVER[ 'REMOTE_ADDR' ];
					$table -> save( $insert );

					if ( $table -> errorInfo ) {
						$this -> _out( 'system', 'Что то пошло не так : ' . $table -> errorInfo );
					}
				}
			}
			else {
				$this -> _out( 'system', 'Пользователь с таким адресом электронной почты не зарегистрирован' );
			}
		}
		if ( !$this -> _out( ) ) {
			$this -> _out( 'ok', 'Инструкции по смене пароля была выслана Вам на электронную почту.' );
			return true;
		}
		return false;
	}


	// восстоновление пароля
	// return : redirect | error | ok
	public function changePass( $args ) {

		global $site_name, $email_admin;
		
		/*
		echo "<pre>";
		var_dump( $args );
		echo "</pre>";
		*/
		if ( !count( $args ) ) {
			$this -> errorInfo[ ] = __FUNCTION__ . " : " . __LINE__ . " > Не переданы аргументы";
			return 'error';
		}

		foreach ( $args as $key => $arg ) {
			$$key = $arg;
		}

		$chp = array( );
		
		if ( isset( $hash ) ) {

			$table = new Table( 'change_pwd' );
			$chp = $table -> select( "SELECT * FROM `change_pwd` WHERE `hash`=:hash && `datestamp`>:dt LIMIT 1",
			array( 'hash' => $hash, 'dt' => time( ) ) );

			if ( !count( $chp ) ) {
				$this -> errorInfo[ ] = __FUNCTION__ . " : " . __LINE__ . " > hash не найден";
				return 'redirect';
			}

		}

		if ( !isset( $req ) ) return 'ok';

		if ( !$pwd ) $this -> _out( 'pwd', "Установите пароль" );
		if ( mb_strlen( $pwd ) < 5 ) $this -> _out( 'pwd', "Пароль не должен быть менее 5ти символов" );
		if ( $pwd != $pwd2 ) $this -> _out( 'pwd2', "Пароль и подтверждение пароля не совпадают" );

		if ( !$this -> _out( ) ) {

			$table = new Table( $this -> table );

			$email = ( !isset( $chp[ 0 ][ 'email' ] ) ) ? null : $chp[ 0 ][ 'email' ];

			$rows = $table -> select( "SELECT * FROM  `" . $this -> table . "` WHERE `email`=:email LIMIT 1",
								array( 'email' => $email ) );

			if ( count( $rows ) ) {
				$row = $rows[ 0 ];

				$table							= new Table( $this -> table );
				$insert							= $table -> getEntity( $row[ 'id' ] );
				$insert -> pass					= md5( $pwd );

				$table -> save( $insert );

				if ( $table -> errorInfo ) {
					$this -> errorInfo[ ] = __FUNCTION__ . " : " . __LINE__ . " > Что то пошло не так: " . $table -> errorInfo;
					return 'error';
				}

				$body = "
" . $rows[ 0 ][ 'title' ] . " " . $rows[ 0 ][ 'stitle' ] . ", добрый день!<br />
Ваш пароль успешно изменен!
<br />
<br />
<b>Ссылка для авторизации: </b> 
<a href='http://" . Utils :: getHost( ) . "/login.html'>http://" . Utils :: getHost( ) . "/login.html</a>
<br />
<b>Адрес электронной почты: </b> 
" . $row[ 'email' ] . "
<br />
<b>Новый пароль: </b> 
" . $pwd . "
<br />
<br />
С уважением, администрация сервиса " . $site_name . ".";

				$body = self :: mailTpl( $body );

				$mailer 				= new Mailer( );
				$mailer	-> IsHTML( true );
				$mailer -> From			= $email_admin;
				$mailer -> FromName		= $site_name;
				$mailer -> Subject		= $site_name . " - смена пароля.";
				$mailer -> Body			= $body;

				$mailer -> AddAddress( trim( $email ) );

				if ( !$mailer -> Send( ) ) {
					$this -> _out( 'system', 'Ошибка: попробуйте повторить это действие через некоторое время' );
				}
				else {
					$table -> execute( "DELETE FROM change_pwd WHERE hash=:hash LIMIT 1", array( 'hash' => $hash ) );
				}
			}
			else {
				$this -> _out( 'system', 'Ссылка устарела' );
			}
		}

		if ( !$this -> _out( ) ) {
			$this -> _out( 'ok', "Пароль, был успешно изменен!" );
			return 'ok';
		}
		return 'error';
	}




	// -- А В Т О Р И З А Ц И Я

	// редактирование профиля
	public function sessEditProfile( $args ) {

		global $site_name, $email_admin;

		if ( !count( $args ) ) {
			$this -> errorInfo[ ] = __FUNCTION__ . " : " . __LINE__ . " > Не переданы аргументы";
			return false;
		}

		foreach ( $args as $key => $arg ) {
			$$key = $arg;
		}

		if ( !$name ) $this -> _out( 'name', "Укажите имя" );
		if ( !$sname ) $this -> _out( 'sname', "Укажите фамилию" );
		if ( !$phone ) $this -> _out( 'phone', "Укажите телефон" );
		// if ( !preg_match( "/^[0-9]$/i", $date ) ) $err .= "Укажите дату рождения";
		// if ( !$photo ) $err .= "Загрузите свою фотографию";
		$send_news = ( !$send_news ) ? 0 : 1;

		if ( !$this -> _out( ) ) {

			$auth = Auth :: getInstance( );
			$sess = $auth -> isAuth( );

			if ( $sess ) {

				$table							= new Table( $this -> table );
				$insert							= $table -> getEntity( $sess[ 'id' ] );
				$insert -> section_id 			= $this -> usersSid( );
				$insert -> title				= $name;
				$insert -> stitle				= $sname;
				$insert -> phone				= $phone;
				$insert -> hb					= $date;
				$insert -> photo				= $photo;
				$insert -> send_news			= $send_news;

				$table -> save( $insert );
				if ( $table -> errorInfo ) {
					$this -> errorInfo[ ] = __FUNCTION__ . " : " . __LINE__ . " > Что то пошло не так: " . $table -> errorInfo;
					return false;
				}

				
				$body = "
" . $name . " " . $sname . ", добрый день!<br />
Профиль успешно изменен!
<br />
<br />
С уважением, администрация сервиса " . $site_name . ".";
			
				$body = self :: mailTpl( $body );
				
				
				$mailer 				= new Mailer( );
				$mailer	-> IsHTML( true );
				$mailer -> From			= $email_admin;
				$mailer -> FromName	= $site_name;
				$mailer -> Subject		= $site_name . " - редактирование профиля.";
				$mailer -> Body			= $body;

				$mailer -> AddAddress( trim( $sess[ 'email' ] ) );

				if ( !$mailer -> Send( ) ) {
					$this -> _out( 'system', "Ошибка: попробуйте повторить это действие через некоторое время" );
				}
				
			}
			else {
				$this -> _out( 'system', "Пользователь не авторизован" );
			}
		}

		if ( !$this -> _out( ) ) {
			$this -> _out( 'ok', "Ваш профиль успешно изменен!" );
			return true;
		}
		return false;
	}



	// сменить пароль
	public function sessChangePass( $args ) {

		if ( !count( $args ) ) {
			$this -> errorInfo[ ] = __FUNCTION__ . " : " . __LINE__ . " > Не переданы аргументы";
			return false;
		}

		foreach ( $args as $key => $arg ) {
			$$key = $arg;
		}

		if ( !$old_pwd ) $this -> _out( 'old_pwd', "Вы не ввели текущий пароль" );

		if ( !$pwd ) $this -> _out( 'pwd', "Установите пароль" );
		if ( mb_strlen( $pwd ) < 5 ) $this -> _out( 'pwd', "Пароль не должен быть менее 5ти символов" );
		if ( $pwd != $pwd2 ) $this -> _out( 'pwd2', "Пароль и подтверждение пароля не совпадают" );

		if ( !$this -> _out( ) ) {

			$auth = Auth :: getInstance( );
			$sess = $auth -> isAuth( );
			if ( $sess ) {

				$table							= new Table( $this -> table );
				$insert							= $table -> getEntity( $sess[ 'id' ] );

				if ( md5( $old_pwd ) != $insert -> pass ) {
					$this -> _out( 'system', "Не правильный текущий пароль" );
					return false;
				}

				$insert -> pass					= md5( $pwd );

				$table -> save( $insert );
				if ( $table -> errorInfo ) {
					$this -> errorInfo[ ] = __FUNCTION__ . " : " . __LINE__ . " > Что то пошло не так: " . $table -> errorInfo;
					return false;
				}
			}
			else {
				$this -> _out( 'system', "Пользователь не авторизован" );
			}
		}

		if ( !$this -> _out( ) ) {
			$this -> _out( 'ok', "Ваш пароль успешно изменен!" );
			return true;
		}
		return false;
	}


	
	
	public static function mailTpl( $body ) {
	
		$str = "
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <title></title>
</head>
<body>
" . $body . "
</body>
</html>";
	
		return $str;
	
	
	}
	
	



	
}



