<?php

class Auth_new extends Auth {

	public $err = array( );
	public $mess = array( );
	public $action = array( ); // location, ...

	public static function getInstance( ) {
		if ( null === self :: $_instance ) {
			self :: $_instance = new self( );
		}
		return self :: $_instance;
	}

	public function __construct( ) {
	}

	public function addErr( $key=null, $val=null ) {
		if ( !$key ) throw new Exception( 'first param `key` - empty' );
		if ( isset( $this -> err[ $key ] ) ) $this -> err[ $key ] .= $val . "<br />";
		else $this -> err[ $key ] = $val;
	}



	// Ф О Р М Ы

	// -- Д О   А В Т О Р И З А Ц И И


	// регистрация
	
	// name		- имя
	// sname		- фамилия
	// phone		- телефон
	// email			- e-mail
	// pwd			- пароль
	// pwd2			- подтверждение пароля
	// date			- дата рождения
	// photo		- фотография
	// promo		- промокод
	// send_news	- подписка на новости
	// consent		- согласие с условиями пользовательского соглашения
	public function register( $args=array( ) ) {

		global $site_name;

		if ( !count( $args ) ) {
			throw new Exception( __FUNCTION__ . " : " . __LINE__ . " > not alias users from catalog_section table" );
			return false;
		}

		$action = 'register';

		foreach ( $args as $key => $arg ) {
			$$key = trim( $arg );
		}


		$table = new Table( $this -> table );
		$intent = $table -> select( "SELECT * FROM  `" . $this -> table . "` WHERE `email`=:email LIMIT 1",
							array( 'email' => $email ) );

		if ( count( $intent ) ) {
			$intent = $intent[ 0 ];
			$this -> addErr( 'email', 'На этот адрес электронной почты уже есть зарегистрированный аккаунт' );
			return false;
		}


		if ( !$pwd ) $this ->addErr( 'pwd', "Установите пароль" );
		if ( mb_strlen( $pwd ) < 5 ) $this -> addErr( 'pwd', "Пароль не должен быть менее 5ти символов" );
		if ( $pwd != $pwd2 ) $this -> addErr( 'pwd2', "Пароль и подтверждение пароля не совпадают" );


		if ( !$this -> _out( ) ) {

			$insert							= $table -> getEntity( );
			$insert = ( is_object( $insert ) ) ? $insert : new stdClass;
			$insert -> section_id 			= $this -> usersSid( );

			$insert -> status				= ( isset( $status ) ) ? $status : '';
			$insert -> email				= ( isset( $email ) ) ? $email : '';
			$insert -> pass					= md5( $pwd );
			$insert -> name					= ( isset( $name ) ) ? $name : '';
			$insert -> phone				= ( isset( $phone ) ) ? $phone : '';
			$insert -> address				= ( isset( $address ) ) ? $address : '';
			$insert -> city					= ( isset( $city ) ) ? $city : '';
			$insert -> status_company		= ( isset( $status_company ) ) ? $status_company : '';
			$insert -> payer				= ( isset( $payer ) ) ? $payer : '';
			$insert -> orgn					= ( isset( $orgn ) ) ? $orgn : '';
			$insert -> legal_address		= ( isset( $legal_address ) ) ? $legal_address : '';
			$insert -> fact_address			= ( isset( $fact_address ) ) ? $fact_address : '';
			$insert -> inn					= ( isset( $inn ) ) ? $inn : '';
			$insert -> kpp					= ( isset( $kpp ) ) ? $kpp : '';
			$insert -> okpo					= ( isset( $okpo ) ) ? $okpo : '';
			$insert -> rs_score				= ( isset( $rs_score ) ) ? $rs_score : '';
			$insert -> kor_score			= ( isset( $kor_score ) ) ? $kor_score : '';
			$insert -> bank					= ( isset( $bank ) ) ? $bank : '';
			$insert -> bik					= ( isset( $bik ) ) ? $bik : '';

			$table -> save( $insert );
			if ( !$table -> errorInfo ) {

				$hash = $this -> _hash( $email, $action );
/*
				$body = "
" . $name . ", добрый день!<br />
Вы получили это письмо, потому что ваш электронный ящик был указан при регистрации на сайте \"" . $site_name . "\",<br />
если вы не регистрировались, просто проигнорируйте данное письмо.<br /><br />
Для активации зарегистрированной учетной записи пройдите по ссылке:<br />
<a href='http://" . Utils :: getHost( ) . "/auth.html?form_action=activation&email=" . $email . "&hash=" . $hash . "' target='_blank'>http://" . Utils :: getHost( ) . "/login.html?hash=" . $hash . "</a>
<br />
<br />
Благодарим Вас за регистрацию.<br />
С уважением, администрация сервиса " . $site_name . ".";
*/

				$emails = $email . ',' . Registry :: __instance( ) -> section_form[ 'email' ];
			
				$ex_emails = explode( ',', $emails );

				foreach ( $ex_emails as $e ) {

					$e = trim( $e );
					if ( !$e ) continue;

					
					$mailer = new Mailer( );
					$mailer	-> IsHTML( true );
					$mailer -> Subject			= Form :: parse_mail_tpl( Registry :: __instance( ) -> section_form[ 'esubject' ], $args );
					$mailer -> Body			= Form :: parse_mail_tpl( Registry :: __instance( ) -> section_form[ 'html' ], $args );

					$mailer -> AddAddress( $e );

					$send = $mailer -> Send( );

					if ( !$send ) {
						$this -> addErr( 'system_message', 'Ошибка ' . __CLASS__ . '#' . __LINE__ );
						return false;
					}
				}
			}
			else {
				$this -> addErr( 'system_message', 'Ошибка DB#' . $table -> errorInfo );
				return false;
			}

		}
		if ( !count( $this -> err ) ) {
			// $this -> mess = "Регистрация прошла успешно! теперь необходимо активировать учетную запись.<br />
			// Инструкции по активации была выслана Вам на электронную почту.";

			$this -> mess = Form :: parse_mail_tpl( Registry :: __instance( ) -> section_form[ 'success_message' ] );

			return true;
		}
		return false;
	}


	
	// авторизация
	// email, pwd
	public function login( $args=array( ) ) {

		if ( !count( $args ) ) {
			throw new Exception( __FUNCTION__ . " : " . __LINE__ . " > Не переданы аргументы" );
			return false;
		}

		foreach ( $args as $key => $arg ) {
			$$key = $arg;
		}

		$table = new Table( $this -> table );
		$rows = $table -> select( "SELECT * FROM  `" . $this -> table . "` WHERE `email`=:email LIMIT 1",
				array( 'email' => $email ) );
		if ( count( $rows ) ) {
			$row = $rows[ 0 ];
			// не верный пароль
			if ( $row[ 'pass' ] != md5( $password ) ) {
				$this -> addErr( 'email', 'Связка логин / пароль неправильные' );
				//$this -> addErr( 'email', 'Связка логин / пароль неправильные ' . __CLASS__ . '#' . __LINE__ );
				return false;
			}
			if ( !$row[ 'visible' ] ) {
				// $this -> addErr( 'email', 'Учетная запись не активирована, проверьте почту.' );
				$this -> addErr( 'email', 'Ваша учетная запись еще не утверждена администратором сайта.' );
				return false;
			}
		}
		// неверный логин
		else {
			$this -> addErr( 'email', 'Связка логин / пароль неправильные' );
			//$this -> addErr( 'email', 'Связка логин / пароль неправильные ' . __CLASS__ . '#' . __LINE__ );
			return false;
		}

		if ( !count( $this -> err ) ) {

				$_SESSION[ 'sess' ] = $row;
				$_SESSION[ 'sess' ][ 'owner' ] = 'self';
				unset( $_SESSION[ 'sess' ][ 'pass' ] );

				// ...
				$orders = new Orders( $_SESSION[ 'sess' ][ 'id' ] );
				//

				$this -> mess = Form :: parse_mail_tpl( Registry :: __instance( ) -> section_form[ 'success_message' ] );
				return true;
		}

		return false;

	}
	
	
	// выход
	public function logout( ) {
		unset( $_SESSION[ 'sess' ] );
	}

	// проверка на авторизацию
	public function isAuth( ) {
		if ( isset( $_SESSION[ 'sess' ] ) ) {
			return $_SESSION[ 'sess' ];
		}
		else return false;
	}

	public function getUserByEmail( $email ) {
		$table = new Table( $this -> table );
		$user = $table -> select( "SELECT * FROM  `" . $this -> table . "` WHERE `email`=:email LIMIT 1",
							array( 'email' => $email ) );
		if ( count( $user ) ) return end( $user );
		else return $user;
	}



}
