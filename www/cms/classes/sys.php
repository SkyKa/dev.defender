<?php

	class Sys {

		public static $access = array( );

		public static $update_server = '';

		public static $sole = 'HSD8oy89sdsndjklhn3uioh2nr03u904j23k4hl';

		public static $status = array(	'update'			=> 0,
										'get'				=> 1,
										'photo'				=> 2,
										'check'				=> 3,
										'whois'				=> 4,
										'seo_wordstat'		=> 5,
										'seo_audit'			=> 6 );

		const TABLE = 'system';

		static public function __instance( ) {
			$methods = get_class_methods( __CLASS__ );
			$method = ( isset( $_POST[ 'status' ] ) ) ? $_POST[ 'status' ] : false;
			if ( isset( $_POST[ 'status' ] ) && $key = array_search( $_POST[ 'status' ], $methods ) !== false ) {
				if ( self :: signature( ) ) {
					try {
						$res = self :: $method( );
					} catch ( Exception $e ) {
						$res = array( 'error' => $e -> getMessage( ) );
					}
					echo self :: result( $res );
				}
			}
		}
	
		// ����������
		static public function update( ) {
			return true;
		}

		// ��������
		static public function get( ) {
			return true;
		}
		
		// ������
		static public function photo( ) {
			
		}

		// ��������
		// domain
		static public function check( ) {
		
			if ( empty( $_POST[ 'domain' ] ) || !$_POST[ 'domain' ] ) {
				throw new Exception( 'param `domain` failed' );
			}

			$start = microtime( true );

			$body = array( );
			$header = null;

			$curlInit = curl_init( $_POST[ 'domain' ] );
			curl_setopt( $curlInit, CURLOPT_CONNECTTIMEOUT, 10 );
			curl_setopt( $curlInit, CURLOPT_TIMEOUT, 10 );
			curl_setopt( $curlInit, CURLOPT_HEADER, true );
			curl_setopt( $curlInit, CURLOPT_NOBODY, true );
			curl_setopt( $curlInit, CURLOPT_RETURNTRANSFER, true );

			$response = curl_exec( $curlInit );
			curl_close( $curlInit );

			if ( $response ) {

				$time = microtime( true ) - $start;
				$body[ 'time' ] = $time;

				$cda = explode( "\n", $response );
				if ( count( $cda ) ) {
					foreach ( $cda as $c ) {
						$ex_c = explode( " ", $c );
						if ( count( $ex_c ) && isset( $ex_c[ 1 ] ) && strpos( $c, 'HTTP/' ) !== false )	{
							$ex_c = explode( " ", $c );
							if ( count( $ex_c ) && isset( $ex_c[ 1 ] ) ) {
								$header = $ex_c[ 1 ];
							}
						}
						else continue;
					}
				}

				$body[ 'domain' ] = $_POST[ 'domain' ];

				return array(	'access' => true,
								'body' => $body,
								'header' => $header );
			}
			return array(	'access' => false,
							'error' => 'curl setopt' );
		}


		// �����
		// domain
		static public function whois( ) {

			if ( empty( $_POST[ 'domain' ] ) || !$_POST[ 'domain' ] ) {
				throw new Exception( 'param `domain` failed' );
			}

			$curlInit = curl_init( $_POST[ 'domain' ] );

			curl_setopt( $curlInit, CURLOPT_URL, "http://www.ripn.net/nic/whois/whois.cgi" );
			curl_setopt( $curlInit, CURLOPT_TIMEOUT, 10 );
			curl_setopt( $curlInit, CURLOPT_POST, 1 );
			curl_setopt( $curlInit, CURLOPT_POSTFIELDS, "Whois=" . $_POST[ 'domain' ] . "&Host=whois.ripn.net" );
			curl_setopt( $curlInit, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt( $curlInit, CURLOPT_CONNECTTIMEOUT, 10 );

			$data = curl_exec( $curlInit );

			curl_close( $curlInit );

			$body = array( );
			if ( $data ) {
					$arr = array( );
					preg_match( '/<pre>(.*)<\/pre>/si', $data, $res );
					if ( isset( $res[ 1 ] ) && $res[ 1 ] ) {
						$result = strip_tags( $res[ 1 ] );
						$ex_result = explode( "\n", $result );

						$cnt = count( $ex_result );

						if ( $cnt ) {
							$i = 0;
							//echo "\n\n";
							foreach ( $ex_result as $res ) {
								++$i;

								if ( $res == '' ) {
									continue;
								}

								$first_symbol = substr( $res, 0, 1 );
								if ( $first_symbol == '%' ) {
									continue;
								}

								if ( strpos( $res, 'Last updated' ) !== false ) {
									continue;
								}

								$ex_param = explode( ':', $res );
								
								if ( count( $ex_param ) ) {
									$param = $ex_param[ 0 ];
									unset( $ex_param[ 0 ] );
									$value = implode( ':', $ex_param );

									$param = trim( $param );
									$value = trim( $value );
									$arr[ $param ][] = $value;
								}
							}
							return array(
								'access' => true,
								'body' => $arr
							);
						}
					}
			}

			return array(
				'access' => false,
				'error' => 'no data'
			);

		}

		
		// ������ wordstat
		static public function seo_wordstat( ) {
			return true;
		}
		
		// ������ �� ������
		static public function seo_audit( ) {
			return true;
		}
		
		
		
		
		
		
		static public function result( $result ) {

			$res =  array(	'request' => $_REQUEST,
							'server' => $_SERVER,
							'result' => $result );

			if ( isset( $_POST[ 'format' ] ) ) {
				if ( $_POST[ 'format' ] == 'xml' ) $res = self :: _xml( $res );
				else $res = self :: _json( $res );
			}
			else $res = serialize( $res );
			return base64_encode( $res );
		}
		

		
		
		protected function _json( $arr ) {
			header( 'Content-type: text/json; charset=utf-8' );
			return json_encode( array( 'data' => $arr ) );
		}


		protected function _xml( $arr ) {
			header( "Content-Type: text/xml; charset=utf-8" );
			return self :: aray_to_xml( $arr, 'data' );
		}
		

		static public function signature( ) {
			self :: access( );
			$var = $_POST;
			$signature = ( isset( $_POST[ 'signature' ] ) ) ? $_POST[ 'signature' ] : false;
			unset( $_POST[ 'signature' ] );
			krsort( $_POST );
			$s = md5( sha1( implode( '-', $_POST ) . self :: $sole ) );
			if ( $signature != $s ) {
				return false;
				//throw new Exception( 'singature false' );
			}
			else return true;
		}
	
	
		static public function access( ) {
			return true;
			/*
			if ( array_search( $_SERVER[ 'REMOTE_ADDR' ], self :: $access ) === false ) {
				throw new Exception( 'access false' );
			}
			else return true;
			*/
		}
	

		static public function send( $domain, $params ) {

			krsort( $params );

			$params[ 'signature' ] = md5( sha1( implode( '-', $params ) . self :: $sole ) );
			$result = file_get_contents( $domain, false, stream_context_create( array(
				'http' => array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => http_build_query( $params )
				)
			) ) );

			return self :: result( $result );
		}
	
	
	
	
	
	
	
	
	
	
	
	
	



		/**
		* ������� ����������� ������� � XML ������
		* �� ���� �������� ������ ��������� ������, �� ������ ���������� � ������� �������� �������� xml
		*
		* @param array $data
		* @param string $rootNodeName - ������ ������ xml.
		* @param SimpleXMLElement $xml - ������������ ����������
		* @return string XML
		*/
		public static function aray_to_xml( $data, $rootNodeName = 'data', $xml=null ) {
			// �������� ����� �������������, �� ������ ����� ����� ��� �� ����� ������
			if ( ini_get( 'zend.ze1_compatibility_mode' ) == 1) {
				ini_set( 'zend.ze1_compatibility_mode', 0 );
			}

			if ( $xml == null ) {
			  $xml = simplexml_load_string( "<?xml version=\"1.0\" encoding=\"utf-8\"?><$rootNodeName />" );
			}

			//���� �������� �������
			foreach( $data as $key => $value ) {
			  // ������ ��������� �������� �������� ����� � XML
			  if ( is_numeric( $key ) ) {
				// ������� ������ �� ����������
				$key = 'item_' . (string) $key;
			  }

			  // ������� �� ��������� �������
			  $key = preg_replace( '/[^a-z0-9_]/i', '', $key );

			  // ���� �������� ������� ����� �������� �������� �� �������� ���� ����������
			  if ( is_array( $value ) ) {
				$node = $xml -> addChild( $key );
				// ����������� �����
				self :: aray_to_xml( $value, $rootNodeName, $node );
			  }
			  else {
				// ��������� ���� ����
				$value = htmlentities( $value );
				$xml -> addChild( $key, $value );
			  }

			}
			// ��������� ������� � ���� ������ ��� ������ XML-������
			$str = $xml -> asXML( );
			return $str;
		}




	}