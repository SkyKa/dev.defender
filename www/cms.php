<?php

error_reporting ( E_ALL );

// Проверка версии php

if ( version_compare( phpversion(), '5.1.0', '<' ) == true ) { die ( 'PHP5.1 < 5.1' ); }

// Константы:

// разделитель
if ( !defined( 'DS' ) ) define ( 'DS', DIRECTORY_SEPARATOR );

// IS_DEV
// IS_TEST
// TS_PRODUCTION

$is_bot = false;
if ( !defined( 'IS_BOT' ) && defined( 'CLI_MODE' ) && !CLI_MODE ) {
	//bf4909d20368d330961e0ff5d8effe6e
	if ( preg_match( "~(Google|Yahoo|Rambler|Bot|Yandex|Spider|Snoopy|Crawler|Finder|Mail|curl)~i", $_SERVER[ 'HTTP_USER_AGENT' ] ) ) $is_bot = true;
	define ( 'IS_BOT', $is_bot );
	define ( 'IS_MAN', !$is_bot );
}


// IS_SMS
// IS_DIRECT
// IS_ADDWORDS
// IS_YANDEX_SEARCH
// IS_GOOGLE_SEARCH
// IS_VK
// IS_ODNOKLASSNIKI
// IS_FACEBOOK


// путь до файлов сайта
$path_parts = pathinfo( __FILE__ );
$site_path = $path_parts[ 'dirname' ] . DS;
define( 'SITE_PATH', $site_path );

$DOCUMENT_ROOT = rtrim( getenv( "DOCUMENT_ROOT" ), "/\\" );
define( 'ROOT', $DOCUMENT_ROOT );

// подпапка в которой стоит CMS
$SUB_FOLDER = str_replace( str_replace( "\\", "/", $DOCUMENT_ROOT ), "", str_replace( "\\", "/", dirname( __FILE__ ) ) );
define( 'SF', $SUB_FOLDER );

// Включение init.php file
include ( SITE_PATH . 'cms' . DS . 'includes' . DS . 'init.php' );
?>